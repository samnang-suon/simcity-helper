package models;

public class MaterialForUI implements Comparable<MaterialForUI> { // source: https://mkyong.com/java/java-object-sorting-example-comparable-and-comparator/
    // ATTRIBUTES
    private String name;
    private String path;

    // CONSTRUCTORS
    public MaterialForUI(String name, String path) {
        this.name = name;
        this.path = path;
    }

    // GETTERS AND SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    // OTHERS
    @Override
    public int compareTo(MaterialForUI o) {
        // https://howtodoinjava.com/java/collections/java-comparable-interface/
        return this.name.compareTo(o.getName());
    }
}
