package models;

public class MaterialQuantity extends Material implements Comparable<MaterialQuantity> {
    // ATTRIBUTES
    private int quantity;
    private int totalTimeToProduceInMinutes;

    // CONSTRUCTORS
    public MaterialQuantity(String name, int quantity) {
        super(name);
        this.quantity = quantity;
        this.totalTimeToProduceInMinutes = this.getTimeToProduceInMinute() * this.quantity;
    }

    // GETTER AND SETTER
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalTimeToProduceInMinutes() {
        return totalTimeToProduceInMinutes;
    }

    public void setTotalTimeToProduceInMinutes(int totalTimeToProduceInMinutes) {
        this.totalTimeToProduceInMinutes = totalTimeToProduceInMinutes;
    }

    // OTHERS
    public void update(int newQuantity) {
        this.quantity = newQuantity;
        this.totalTimeToProduceInMinutes = this.getTimeToProduceInMinute() * this.quantity;
    }
    @Override
    public String toString() {
        return "MaterialQuantity{" +
                "name=" + this.getName()  + "," +
                "timeToProduceInMinute=" + this.getTimeToProduceInMinute() + "," +
                "quantity=" + quantity + "," +
                "totalTimeToProduce=" + this.totalTimeToProduceInMinutes +
                '}';
    }

    @Override
    public int compareTo(MaterialQuantity o) {
        // First, sort by duration DESC
        if (this.totalTimeToProduceInMinutes > o.getTotalTimeToProduceInMinutes()) {
            return -1;
        }
        if (this.totalTimeToProduceInMinutes < o.getTotalTimeToProduceInMinutes()) {
            return 1;
        }

        // Second, sort by quantity DESC
        if(this.quantity > o.getQuantity()) {
            return -1;
        }
        if(this.quantity < o.getQuantity()) {
            return 1;
        }

        return 0;
    }
}
