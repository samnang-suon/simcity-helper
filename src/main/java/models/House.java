package models;

import java.util.ArrayList;

public class House {
    // ATTRIBUTES
    private String name;
    private ArrayList<MaterialQuantity> materialQuantities;
    private int totalMaterialProduceTimeInMinute;

    // CONSTRUCTORS
    public House(String name) {
        this.name = name;
        this.materialQuantities = new ArrayList<>();
        this.totalMaterialProduceTimeInMinute = 0;
    }

    public House() {
        this.name = "";
        this.materialQuantities = new ArrayList<>();
        this.totalMaterialProduceTimeInMinute = 0;
    }

    // GETTER AND SETTER
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<MaterialQuantity> getMaterialQuantities() {
        return materialQuantities;
    }

    public void setMaterialQuantities(ArrayList<MaterialQuantity> materialQuantities) {
        this.materialQuantities = materialQuantities;
    }

    public int getTotalMaterialProduceTimeInMinute() {
        return totalMaterialProduceTimeInMinute;
    }

    public void setTotalMaterialProduceTimeInMinute(int totalMaterialProduceTimeInMinute) {
        this.totalMaterialProduceTimeInMinute = totalMaterialProduceTimeInMinute;
    }

    // OTHER
    public void add(MaterialQuantity pMaterialQuantity) {
        this.materialQuantities.add(pMaterialQuantity);
        this.totalMaterialProduceTimeInMinute += pMaterialQuantity.getTimeToProduceInMinute() * pMaterialQuantity.getQuantity();
    }
    public void add(MaterialQuantity tempMaterial, int produceTime) {
        this.materialQuantities.add(tempMaterial);
        this.totalMaterialProduceTimeInMinute += produceTime * tempMaterial.getQuantity();
    }

    @Override
    public String toString() {
        return "House{" +
                "name=" + name + "," +
                "totalMaterialProduceTimeInMinute=" + totalMaterialProduceTimeInMinute +
                '}';
    }
}
