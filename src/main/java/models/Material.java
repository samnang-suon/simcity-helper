package models;

import java.util.ArrayList;

public class Material {
    // ATTRIBUTES
    private String name;
    private int timeToProduceInMinute;
    private ArrayList<Material> materials;

    // CONSTRUCTORS
    public Material(String name) {
        this.materials = initMaterials();
        this.timeToProduceInMinute = getTimeToProduceInMinuteByName(name);
        this.name = name;
    }
    public Material(String name, int timeToProduceInMinute) {
        this.name = name;
        this.timeToProduceInMinute = timeToProduceInMinute;
    }

    // GETTER AND SETTER
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTimeToProduceInMinute() {
        return timeToProduceInMinute;
    }

    public void setTimeToProduceInMinute(int timeToProduceInMinute) {
        this.timeToProduceInMinute = timeToProduceInMinute;
    }

    // OTHERS
    public int getTimeToProduceInMinuteByName(String name) {
        for(Material material : this.materials) {
            if(material.getName().toLowerCase().contains(name.toLowerCase())) {
                return material.getTimeToProduceInMinute();
            }
        }

        return -999;
    }
    private ArrayList<Material> initMaterials() {
        ArrayList<Material> materials = new ArrayList<>();

        // Raw Materials
        materials.add(new Material("metal", 1));
        materials.add(new Material("wood", 3));
        materials.add(new Material("plastic", 9));
        materials.add(new Material("seed", 20));
        materials.add(new Material("mineral", 30));
        materials.add(new Material("chemical", 120));
        materials.add(new Material("textile", 180));
        materials.add(new Material("sugar_and_spice", 240));
        materials.add(new Material("glass", 300));
        materials.add(new Material("animal_feed", 360));
        materials.add(new Material("electrical_component", 420));

        // Building Supplies Store
        materials.add(new Material("nail", 5));
        materials.add(new Material("plank", 30));
        materials.add(new Material("brick", 20));
        materials.add(new Material("cement", 50));
        materials.add(new Material("glue", 60));
        materials.add(new Material("paint", 60));

        // Hardware Store
        materials.add(new Material("hammer", 14));
        materials.add(new Material("measuring_tape", 20));
        materials.add(new Material("shovel", 30));
        materials.add(new Material("cooking_utensil", 45));
        materials.add(new Material("ladder", 60));
        materials.add(new Material("drill", 120));

        // Farmer' Market
        materials.add(new Material("vegetable", 20));
        materials.add(new Material("flour_bag", 30));
        materials.add(new Material("fruit_and_berry", 90));
        materials.add(new Material("cream", 75));
        materials.add(new Material("corn", 60));
        materials.add(new Material("cheese", 105));
        materials.add(new Material("beef", 150));

        // Furniture Store
        materials.add(new Material("chair", 20));
        materials.add(new Material("table", 30));
        materials.add(new Material("home_textile", 75));
        materials.add(new Material("cupboard", 45));
        materials.add(new Material("couch", 150));

        // Gardening Supplies
        materials.add(new Material("grass", 30));
        materials.add(new Material("tree_sapling", 90));
        materials.add(new Material("garden_furniture", 135));
        materials.add(new Material("fire_pit", 240));
        materials.add(new Material("lawn_mower", 120));
        materials.add(new Material("garden_gnome", 90));

        // Donut Shop
        materials.add(new Material("donut", 45));
        materials.add(new Material("green_smoothie", 30));
        materials.add(new Material("bread_roll", 60));
        materials.add(new Material("cherry_cheesecake", 90));
        materials.add(new Material("frozen_yogurt", 240));
        materials.add(new Material("coffee", 60));

        // Fashion Store
        materials.add(new Material("cap", 60));
        materials.add(new Material("shoe", 75));
        materials.add(new Material("watch", 90));
        materials.add(new Material("business_suit", 210));
        materials.add(new Material("backpack", 150));

        // Fast Food Restaurant
        materials.add(new Material("ice_cream_sandwich", 14));
        materials.add(new Material("pizza", 24));
        materials.add(new Material("burger", 35));
        materials.add(new Material("cheese_fries", 20));
        materials.add(new Material("lemonade_bottle", 60));
        materials.add(new Material("popcorn", 30));

        // Home Appliances
        materials.add(new Material("bbq_grill", 165));
        materials.add(new Material("refrigerator", 210));
        materials.add(new Material("lighting_system", 105));
        materials.add(new Material("tv", 150));
        materials.add(new Material("microwave_oven", 120));

        // Coconut Farm
        materials.add(new Material("coconut", 6));
        materials.add(new Material("coconut_oil", 20));
        materials.add(new Material("face_cream", 90));
        materials.add(new Material("tropical_drink", 250));

        // Mulberry Grove
        materials.add(new Material("silk", 6));

        // Silk Store
        materials.add(new Material("string", 20));
        materials.add(new Material("fan", 150));
        materials.add(new Material("robe", 240));

        return materials;
    }
}
