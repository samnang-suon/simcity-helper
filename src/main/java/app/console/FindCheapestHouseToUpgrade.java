package app.console;

import models.House;
import models.Material;
import models.MaterialQuantity;

import java.util.ArrayList;
import java.util.Scanner;

public class FindCheapestHouseToUpgrade {
    public static void main(String[] args) {
        ArrayList<Material> materialArrayList = initMaterials();
        Scanner readUserInput = new Scanner(System.in);
        ArrayList<House> houses = new ArrayList<>();

        boolean exitHouseLoop = false;
        do {
            System.out.println("Enter house friendly name");
            System.out.print(">>> ");
            String houseName = readUserInput.nextLine();
            House tempHouse = new House(houseName);
            boolean exitMaterialLoop = false;
            do {
                System.out.println("Enter material follow by quantity (ex: Metal 5)");
                System.out.println("Enter 'done' to stop adding materials");
                System.out.print(">>> ");
                String material = readUserInput.nextLine();

                // Parse user input
                String[] parsedMaterials = material.split(" ");
                if (parsedMaterials.length == 2) {
                    String inputMaterialName = parsedMaterials[0].toLowerCase();
                    int inputMaterialQuantity = Integer.parseInt(parsedMaterials[1]);
                    int produceTime = getProduceTimeByMaterial(inputMaterialName, materialArrayList);
                    tempHouse.add(new MaterialQuantity(inputMaterialName, inputMaterialQuantity), produceTime);
                }

                // Check loop condition
                if (parsedMaterials.length > 1) {
                    exitMaterialLoop = false;
                } else {
                    exitMaterialLoop = true;
                }
            } while (!exitMaterialLoop);
            houses.add(tempHouse);

            System.out.println("Do you want to add another house (y/n)?");
            System.out.print(">>> ");
            String tempUserAnswer = readUserInput.nextLine();
            if(tempUserAnswer.equalsIgnoreCase("n") || tempUserAnswer.equalsIgnoreCase("no")) {
                exitHouseLoop = true;
            } else {
                exitHouseLoop = false;
            }
        } while (!exitHouseLoop);


        // Show entered houses and its needed materials
        System.out.println("==================== RESULT REPORT ====================");
        for(House house : houses) {
            System.out.println("House name: " + house.getName());
            System.out.println("House total produce duration: " + house.getTotalMaterialProduceTimeInMinute() + " minutes");
            for(MaterialQuantity materialQuantity : house.getMaterialQuantities()) {
                System.out.println(materialQuantity.getName() + " :: "+ materialQuantity.getQuantity());
            }
            System.out.println("==============================");
        }
    }

    private static int getProduceTimeByMaterial(String inputMaterialName, ArrayList<Material> materialArrayList) {
        for(Material material : materialArrayList) {
            if(inputMaterialName.equalsIgnoreCase(material.getName())) {
                return material.getTimeToProduceInMinute();
            }
        }

        return -999;
    }

    static ArrayList<Material> initMaterials() {
        ArrayList<Material> materials = new ArrayList<>();

        // Raw Materials
        materials.add(new Material("metal", 1));
        materials.add(new Material("wood", 3));
        materials.add(new Material("plastic", 9));
        materials.add(new Material("seed", 20));
        materials.add(new Material("mineral", 30));
        materials.add(new Material("chemical", 120));
        materials.add(new Material("textile", 180));
        materials.add(new Material("sugar_and_spice", 240));
        materials.add(new Material("glass", 300));
        materials.add(new Material("animal_feed", 360));
        materials.add(new Material("electrical_component", 420));

        // Building Supplies Store
        materials.add(new Material("nail", 5));
        materials.add(new Material("plank", 30));
        materials.add(new Material("brick", 20));
        materials.add(new Material("cement", 50));
        materials.add(new Material("glue", 60));
        materials.add(new Material("paint", 60));

        // Hardware Store
        materials.add(new Material("hammer", 14));
        materials.add(new Material("measuring_tape", 20));
        materials.add(new Material("shovel", 30));
        materials.add(new Material("cooking_utensil", 45));
        materials.add(new Material("drill", 120));

        // Farmer' Market
        materials.add(new Material("vegetable", 20));
        materials.add(new Material("flour_bag", 30));
        materials.add(new Material("fruit_and_berry", 90));
        materials.add(new Material("cream", 75));
        materials.add(new Material("corn", 60));
        materials.add(new Material("cheese", 105));
        materials.add(new Material("beef", 150));

        // Furniture Store
        materials.add(new Material("chair", 20));
        materials.add(new Material("table", 30));
        materials.add(new Material("home_textile", 75));
        materials.add(new Material("cupboard", 45));
        materials.add(new Material("couch", 150));

        // Gardening Supplies
        materials.add(new Material("grass", 30));
        materials.add(new Material("tree_sapling", 90));
        materials.add(new Material("garden_furniture", 135));
        materials.add(new Material("fire_pit", 240));
        materials.add(new Material("lawn_mower", 120));
        materials.add(new Material("garden_gnome", 90));

        // Donut Shop
        materials.add(new Material("donut", 45));
        materials.add(new Material("green_smoothie", 30));
        materials.add(new Material("bread_roll", 60));
        materials.add(new Material("cherry_cheesecake", 90));
        materials.add(new Material("frozen_yogurt", 240));
        materials.add(new Material("coffee", 60));

        // Fashion Store
        materials.add(new Material("cap", 60));
        materials.add(new Material("shoe", 75));
        materials.add(new Material("watch", 90));
        materials.add(new Material("business_suit", 210));
        materials.add(new Material("backpack", 150));

        // Fast Food Restaurant
        materials.add(new Material("ice_cream_sandwich", 14));
        materials.add(new Material("pizza", 24));
        materials.add(new Material("burger", 35));
        materials.add(new Material("cheese_fries", 20));
        materials.add(new Material("lemonade_bottle", 60));
        materials.add(new Material("popcorn", 30));

        // Home Appliances
        materials.add(new Material("bbq_grill", 165));
        materials.add(new Material("refrigerator", 210));
        materials.add(new Material("lighting_system", 105));
        materials.add(new Material("tv", 150));
        materials.add(new Material("microwave_oven", 120));

        // Other Raw Materials
        materials.add(new Material("coconut", 6));

        return materials;
    }
}
