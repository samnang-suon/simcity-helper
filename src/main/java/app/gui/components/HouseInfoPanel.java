package app.gui.components;

import javax.swing.*;
import java.awt.*;

public class HouseInfoPanel extends JPanel {
    // ATTRIBUTES
    private JLabel houseNameLbl;
    private JTextField houseNameTxt;

    // CONSTRUCTORS
    public HouseInfoPanel(Font pFont) {
        this.houseNameLbl = new JLabel("House friendly name:");
        this.houseNameLbl.setFont(pFont);
        this.houseNameTxt = new JTextField(20);
        this.houseNameTxt.setFont(pFont);
        this.add(this.houseNameLbl);
        this.add(this.houseNameTxt);
    }

    // GETTERS AND SETTERS
    public JLabel getHouseNameLbl() {
        return houseNameLbl;
    }

    public void setHouseNameLbl(JLabel houseNameLbl) {
        this.houseNameLbl = houseNameLbl;
    }

    public JTextField getHouseNameTxt() {
        return houseNameTxt;
    }

    public void setHouseNameTxt(JTextField houseNameTxt) {
        this.houseNameTxt = houseNameTxt;
    }
}
