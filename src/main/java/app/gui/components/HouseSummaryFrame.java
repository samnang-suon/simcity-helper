package app.gui.components;

import models.House;
import models.MaterialQuantity;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HouseSummaryFrame extends JFrame implements ActionListener {
    // ATTRIBUTES
    JTextPane textPane;

    // CONSTRUCTORS
    public HouseSummaryFrame() {
        this.setTitle("House Summary Overview");
        this.setSize(400, 600);

        // https://stackoverflow.com/questions/9543320/how-to-position-the-form-in-the-center-screen/25141917#25141917
        Dimension windowSize = this.getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        int dx = centerPoint.x + (windowSize.width);
        int dy = centerPoint.y - (windowSize.height/2);
        this.setLocation(dx, dy);

        // https://stackoverflow.com/questions/4059198/jtextpane-appending-a-new-string/4059365
        this.textPane = new JTextPane();
        this.textPane.setEditable(false);
        this.textPane.setFont(new Font(Font.MONOSPACED, Font.PLAIN,  24));
        /*
        this.textPane.setText( "original text" );
        StyledDocument doc = this.textPane.getStyledDocument();
        SimpleAttributeSet keyWord = new SimpleAttributeSet();
        StyleConstants.setForeground(keyWord, Color.RED);
        StyleConstants.setBackground(keyWord, Color.YELLOW);
        StyleConstants.setBold(keyWord, true);
        try
        {
            doc.insertString(0, "Start of text\n", null );
            doc.insertString(doc.getLength(), "\nEnd of text", keyWord );
        }
        catch(Exception e) { System.out.println(e); }
        */
        JScrollPane jsp = new JScrollPane(this.textPane);
        this.add(jsp);

        JButton saveToFileBtn = new JButton("Save to file");
        saveToFileBtn.setActionCommand("SAVE_TO_FILE");
        saveToFileBtn.addActionListener(this);
        this.add(saveToFileBtn, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    // GETTERS AND SETTERS

    // OTHERS
    public void add(House pHouse) {
        StyledDocument doc = this.textPane.getStyledDocument();
        try {
            doc.insertString(doc.getLength(), pHouse.toString() + "\n", null);
            for(MaterialQuantity materialQuantity : pHouse.getMaterialQuantities()) {
                doc.insertString(doc.getLength(), "\t\t" + materialQuantity.toString() + "\n", null);
            }
            doc.insertString(doc.getLength(), "==============================\n", null);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("ActionEvent: " + e);
        switch(e.getActionCommand()) {
            case "SAVE_TO_FILE":
                try {
                    writeContentToFile();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                break;
        }
    }

    private void writeContentToFile() throws IOException {
        // Create file by date and time
        // source: https://www.javatpoint.com/java-get-current-date
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        String currentDateTimeFormat = dtf.format(now);
        System.out.println(currentDateTimeFormat);

        String filename = currentDateTimeFormat + "_Saved_Content.txt";
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename));
        bufferedWriter.write(this.textPane.getText());
        bufferedWriter.write("\n");
        bufferedWriter.close();

        JOptionPane.showMessageDialog(this,
                "'" + filename + "' has been created.",
                "Content Saved",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
