package app.gui.components;

import models.MaterialForUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class MaterialSelectionPanel extends JPanel implements ActionListener {
    // ATTRIBUTES
    ArrayList<MaterialForUI> allResourceFolderImages;
    JButton[] buttons;
    String selectedButton;
    String selectedMaterialName;

    // CONSTRUCTORS
    public MaterialSelectionPanel() {
        try {
            this.allResourceFolderImages = getAllResourceFiles();
            System.out.println("IMAGES ICON COUNT = " + this.allResourceFolderImages.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("============================== SORTED LIST ==============================");
        Collections.sort(this.allResourceFolderImages);
        for(MaterialForUI sortedList : this.allResourceFolderImages) {
            System.out.println(sortedList.getName());
        }

        // GridLayout gridLayout = new GridLayout(10, 7, 25, 25);
        // this.setLayout(gridLayout);
        this.buttons = new JButton[this.allResourceFolderImages.size()];
        for (int i = 0; i < this.buttons.length; i++) {
            this.buttons[i] = new JButton();
            if (i < this.allResourceFolderImages.size()) {
                // Read images from resources folder
                // https://stackoverflow.com/questions/15749192/how-do-i-load-a-file-from-resource-folder
                URL imageURL = getClass().getClassLoader().getResource(this.allResourceFolderImages.get(i).getPath());
                ImageIcon imageIcon = new ImageIcon(imageURL);

                // Scale icon
                // https://stackoverflow.com/questions/6714045/how-to-resize-jlabel-imageicon/18335435#18335435
                Image tempImage = imageIcon.getImage();
                Image scaledImageIcon = tempImage.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way

                this.buttons[i].setIcon(new ImageIcon(scaledImageIcon));
                this.buttons[i].setActionCommand(this.allResourceFolderImages.get(i).getPath());
                this.add(this.buttons[i]);
                this.buttons[i].addActionListener(this);
            }
        }

        resetButtonColor();
    }

    // https://www.logicbig.com/how-to/java/list-all-files-in-resouce-folder.html
    private ArrayList<MaterialForUI> getAllResourceFiles() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource("images");
        String path = url.getPath();
        ArrayList<String> allResourceFiles = new ArrayList<>();
        getFilenameRecursively(new File(path), allResourceFiles);
        ArrayList<MaterialForUI> output = new ArrayList<>();
        System.out.println("==================== LIST OF RESOURCE FOLDER FILES ====================");
        for (String imagePath : allResourceFiles) {
            //// Image Path
            System.out.println(imagePath);

            //// Image name
            String[] materialName = imagePath.split("\\\\");
            System.out.println("Size=" + materialName.length);
            for(String mN : materialName) {
                System.out.println("\t\t" + mN);
            }
            String tempNameToKeep = materialName[materialName.length-1];
            tempNameToKeep = tempNameToKeep.toLowerCase().replace(".png", "");
            System.out.println("TO_KEEP: " + tempNameToKeep.toLowerCase());

            output.add(new MaterialForUI(tempNameToKeep.toLowerCase(), imagePath));
        }

        return output;
    }

    private void getFilenameRecursively(File folder, ArrayList<String> allResourceFiles) throws IOException {
        File[] tempFiles = folder.listFiles();
        for (File tempFile : tempFiles) {
            if (tempFile.isDirectory()) {
                getFilenameRecursively(tempFile, allResourceFiles);
            } else {
                allResourceFiles.add(getImageFromResourceFolder(tempFile));
            }
        }
    }

    private String getImageFromResourceFolder(File tempFile) {
        String imageFromResourceFolder = tempFile.getPath();
        int indexOfResourceMain = imageFromResourceFolder.indexOf("images");
        String[] imageFromResourceFolderArray = imageFromResourceFolder.split("");
        String tempBuild = "";
        for (int i = 0; i < imageFromResourceFolderArray.length; i++) {
            if (i >= indexOfResourceMain) {
                tempBuild += imageFromResourceFolderArray[i];
            }
        }
        return tempBuild;
    }

    // GETTERS AND SETTERS
    public ArrayList<MaterialForUI> getAllResourceFolderImages() {
        return allResourceFolderImages;
    }

    public void setAllResourceFolderImages(ArrayList<MaterialForUI> allResourceFolderImages) {
        this.allResourceFolderImages = allResourceFolderImages;
    }

    public JButton[] getButtons() {
        return buttons;
    }

    public void setButtons(JButton[] buttons) {
        this.buttons = buttons;
    }

    public String getSelectedButton() {
        return selectedButton;
    }

    public void setSelectedButton(String selectedButton) {
        this.selectedButton = selectedButton;
    }

    public String getSelectedMaterialName() {
        return selectedMaterialName;
    }

    public void setSelectedMaterialName(String selectedMaterialName) {
        this.selectedMaterialName = selectedMaterialName;
    }

    // OTHERS
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("==================== MaterialSelectionPanel.ActionPerformed() ====================");
        System.out.println("ActionEvent: " + e + "\n");

        // Reset background color for all buttons
        resetButtonColor();

        // Highlight button background
        for (JButton btn : this.buttons) {
            if (btn.getActionCommand().equalsIgnoreCase(e.getActionCommand())) {
                btn.setBackground(Color.YELLOW);
                this.selectedButton = btn.getActionCommand();

                // https://stackoverflow.com/questions/23751618/how-to-split-a-java-string-at-backslash
                System.out.println("SelectedButton: " + this.selectedButton);
                String[] filenameArray = this.selectedButton.split("\\\\");
                for (String filename : filenameArray) {
                    System.out.println(filename);
                }
                String tempName = filenameArray[filenameArray.length-1];
                tempName = tempName.toLowerCase().replace(".png", "");
                System.out.println("Last word in path: " + tempName);
                this.selectedMaterialName = tempName;
            }
        }
    }

    public void resetButtonColor() {
        for (JButton btn : this.buttons) {
            // btn.setBackground(new Color(238, 238, 238));
            // https://stackoverflow.com/questions/4129666/how-to-convert-hex-to-rgb-using-java
            btn.setBackground(Color.decode("#f9faff"));
        }
    }
}
