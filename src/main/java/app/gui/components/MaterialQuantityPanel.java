package app.gui.components;

import javax.swing.*;
import java.awt.*;

public class MaterialQuantityPanel extends JPanel {
    // ATTRIBUTES
    JLabel materialQuantityLbl;
    JSpinner spinner;

    // CONSTRUCTORS
    public MaterialQuantityPanel(Font pFont) {
        this.materialQuantityLbl = new JLabel("Material quantity: ");
        this.materialQuantityLbl.setFont(pFont);
        SpinnerModel model = new SpinnerNumberModel(1, 1, 25, 1);
        this.spinner = new JSpinner(model);
        this.spinner.setFont(pFont);
        this.add(this.materialQuantityLbl);
        this.add(this.spinner);
    }

    // GETTERS AND SETTERS
    public JLabel getMaterialQuantityLbl() {
        return materialQuantityLbl;
    }

    public void setMaterialQuantityLbl(JLabel materialQuantityLbl) {
        this.materialQuantityLbl = materialQuantityLbl;
    }

    public JSpinner getSpinner() {
        return spinner;
    }

    public void setSpinner(JSpinner spinner) {
        this.spinner = spinner;
    }
}
