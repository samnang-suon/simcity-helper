package app.gui;

import app.gui.components.HouseInfoPanel;
import app.gui.components.HouseSummaryFrame;
import app.gui.components.MaterialQuantityPanel;
import app.gui.components.MaterialSelectionPanel;
import models.House;
import models.MaterialQuantity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collections;

public class SwingApp extends JFrame implements ActionListener, WindowListener {
    // ATTRIBUTES
    private HouseInfoPanel houseInfoPanel;
    private MaterialQuantityPanel materialPanel;
    private MaterialSelectionPanel materialSelectionPanel;
    private JButton addMaterialBtn;
    private JButton addNewHouseBtn;
    private Font globalFont;
    private ArrayList<House> localHouses;
    private int houseCounter;
    private HouseSummaryFrame houseSummaryFrame;

    // CONSTRUCTORS
    public SwingApp() {
        this.setTitle("Simcity: build-It Helper");
        this.localHouses = new ArrayList<>();
        this.localHouses.add(new House());
        this.houseSummaryFrame = new HouseSummaryFrame();

        // https://stackoverflow.com/questions/11570356/jframe-in-full-screen-java
        // this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setSize(1000, 650);

        // http://www.java2s.com/Tutorials/Java/Java_Swing/1520__Java_Swing_Font.htm
        this.globalFont = new Font(Font.MONOSPACED, Font.BOLD,  24);
        this.setFont(this.globalFont);

        this.houseInfoPanel = new HouseInfoPanel(this.globalFont);
        this.materialPanel = new MaterialQuantityPanel(this.globalFont);
        JPanel tempPanel = new JPanel();
        tempPanel.add(this.houseInfoPanel);
        tempPanel.add(this.materialPanel);
        this.add(tempPanel, BorderLayout.NORTH);

        this.materialSelectionPanel = new MaterialSelectionPanel();
        this.add(this.materialSelectionPanel, BorderLayout.CENTER);

        this.addMaterialBtn = new JButton("Add material");
        this.addMaterialBtn.setActionCommand("ADD_MATERIAL");
        this.addMaterialBtn.setFont(this.globalFont);
        this.addMaterialBtn.setBackground(Color.decode("#0275d8"));
        this.addMaterialBtn.addActionListener(this);
        this.addNewHouseBtn = new JButton("Add new house");
        this.addNewHouseBtn.setActionCommand("ADD_NEW_HOUSE");
        this.addNewHouseBtn.setFont(this.globalFont);
        this.addNewHouseBtn.setBackground(Color.decode("#5cb85c"));
        this.addNewHouseBtn.addActionListener(this);
        JPanel frameButtonPanel = new JPanel();
        frameButtonPanel.add(this.addMaterialBtn);
        frameButtonPanel.add(this.addNewHouseBtn);
        this.add(frameButtonPanel, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(this);
    }

    // GETTERS AND SETTERS

    // OTHERS
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("==================== SwingApp.JFrame.ActionPerformed() ====================");
        System.out.println("ActionEvent: " + e);

        switch(e.getActionCommand()) {
            case "ADD_MATERIAL":
                System.out.println("!!! ADD_MATERIAL !!!");

                // House properties
                String houseName = this.houseInfoPanel.getHouseNameTxt().getText();
                System.out.println("getHouseNameTxt: " + houseName);
                if (houseName.length() > 1) {
                    this.localHouses.get(this.houseCounter).setName(houseName);
                    this.houseInfoPanel.getHouseNameTxt().setBackground(Color.WHITE);
                } else {
                    JOptionPane.showMessageDialog(null, "House name cannot be empty.", "ERROR", JOptionPane.ERROR_MESSAGE);
                    this.houseInfoPanel.getHouseNameTxt().requestFocus(); // https://stackoverflow.com/questions/6723257/how-to-set-focus-on-jtextfield/6723316
                    this.houseInfoPanel.getHouseNameTxt().setBackground(Color.RED);
                    return;
                }

                // Material properties
                int materialQuantity = Integer.parseInt(String.valueOf(this.materialPanel.getSpinner().getValue()));
                System.out.println("getValue : " + materialQuantity);
                String selectedMaterialName = this.materialSelectionPanel.getSelectedMaterialName();
                System.out.println("getSelectedButton: " + selectedMaterialName);
                if(selectedMaterialName != null) {
                    // Check if material already exist
                    boolean isMaterialDuplicate = false;
                    int duplicateMaterialIndex = 0;
                    for(int i=0; i < this.localHouses.get(this.houseCounter).getMaterialQuantities().size(); i++) {
                        if(this.localHouses.get(this.houseCounter).getMaterialQuantities().get(i).getName().equalsIgnoreCase(selectedMaterialName)) {
                            System.out.println("Duplicate found: " + selectedMaterialName);
                            isMaterialDuplicate = true;
                            duplicateMaterialIndex = i;
                            break;
                        }
                    }

                    if(isMaterialDuplicate) {
                        int updatedQuantity = this.localHouses.get(this.houseCounter).getMaterialQuantities().get(duplicateMaterialIndex).getQuantity() + materialQuantity;
                        this.localHouses.get(this.houseCounter).getMaterialQuantities().get(duplicateMaterialIndex).update(updatedQuantity);
                        JOptionPane.showMessageDialog(null,
                                selectedMaterialName + " 'qty' was update to '" + updatedQuantity + "'",
                                "INFO",
                                JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        MaterialQuantity tempMaterialQuantity = new MaterialQuantity(selectedMaterialName, materialQuantity);
                        this.localHouses.get(this.houseCounter).add(tempMaterialQuantity);
                        int tempTotalTimeToProduce = materialQuantity * tempMaterialQuantity.getTimeToProduceInMinute();
                        JOptionPane.showMessageDialog(null,
                                selectedMaterialName + " : " + materialQuantity + "(qty) X " + tempMaterialQuantity.getTimeToProduceInMinute() + "(min) = " + tempTotalTimeToProduce + "(min) was added.",
                                "INFO",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Please select one material.",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                }
                this.materialPanel.getSpinner().setValue(1);
                this.materialSelectionPanel.resetButtonColor();
                break;
            case "ADD_NEW_HOUSE":
                System.out.println("!!! ADD_NEW_HOUSE !!!");

                // Sort Materials
                Collections.sort(this.localHouses.get(this.houseCounter).getMaterialQuantities());

                this.houseSummaryFrame.add(this.localHouses.get(this.houseCounter));
                this.houseInfoPanel.getHouseNameTxt().setText("");
                this.houseInfoPanel.getHouseNameTxt().setBackground(Color.WHITE);
                this.materialPanel.getSpinner().setValue(1);
                this.houseCounter++;
                this.localHouses.add(new House());
                this.materialSelectionPanel.resetButtonColor();
                this.houseInfoPanel.getHouseNameTxt().requestFocus();
                break;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("==================== WINDOWS IS CLOSING ====================");
        for(House tempHouse : this.localHouses) {
            System.out.println(tempHouse);
            for(MaterialQuantity tempMQ : tempHouse.getMaterialQuantities()) {
                System.out.println(tempMQ);
            }
            System.out.println("\n");
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("==================== WINDOWS HAS CLOSED ====================");
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    // MAIN PROGRAM
    public static void main(String[] args) {
        SwingApp swingApp = new SwingApp();
    }
}
